%define buildpath %(echo %{name} | sed 's/perl-//')

Name:           perl-Module-Install-ManifestSkip
Version:        0.24
Release:        14
Summary:        Generate a MANIFEST.SKIP file
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Module-Install-ManifestSkip
Source0:        https://cpan.metacpan.org/authors/id/I/IN/INGY/Module-Install-ManifestSkip-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl-interpreter perl-generators perl(strict) perl(warnings)
BuildRequires:  perl(base) perl(Module::Install::Base) perl(Test::More)
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.30

Requires:       perl(Module::Manifest::Skip) >= 0.18
Requires:       perl(warnings)

%description
This module generates a MANIFEST.SKIP file for you (using
Module::Manifest::Skip) that contains the common files that people do not
want in their MANIFEST files. The SKIP file is generated each time that you
(the module author) run Makefile.PL.

%package	help
Summary:	help package of %{name} that include files

%description	help
document files for %{name}

%prep
%autosetup -n %{buildpath}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc CONTRIBUTING README Changes
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.24-14
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.24-13
- Package init

